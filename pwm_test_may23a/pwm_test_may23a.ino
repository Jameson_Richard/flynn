/********************************************************
 * setup() function
 *
 * nothing
 *******************************************************/

void setup()
{

}

/****************************************************************
 * loop() function
 *
 * Transfers the input of 5 digital pins to 5 other digital pins.
 * Digital pins 2 through 6 are the input pins, digital pins 8 through 12
 * are the output pins. D2->D8, D3->D9, D4->D10, D5->D11, D6->D12
 ****************************************************************/
void loop()
{
  PORTB = PIND >> 2;  // right shift the value of the PIND register 2 bits.
}
